<?php

/**
 * @file
 * Caches responses for anonymous users, request and response policies allowing.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Implements hook_help().
 */
function page_cache_dnt_aware_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.page_cache_dnt_aware':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Do-Not-Track Aware Internal Page Cache module caches pages for anonymous users in the database. For more information, see the <a href=":pagecache-documentation">online documentation for the Internal Page Cache module</a>.', [':pagecache-documentation' => 'https://www.drupal.org/documentation/modules/internal_page_cache']) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Speeding up your site') . '</dt>';
      $output .= '<dd>' . t('Pages requested by anonymous users are stored the first time they are requested and then are reused. Depending on your site configuration and the amount of your web traffic tied to anonymous visitors, the caching system may significantly increase the speed of your site.') . '</dd>';
      $output .= '<dd>' . t('Pages are usually identical for all anonymous users, while they can be personalized for each authenticated user. This is why entire pages can be cached for anonymous users, whereas they will have to be rebuilt for every authenticated user.') . '</dd>';
      $output .= '<dd>' . t('To speed up your site for authenticated users, see the <a href=":dynamic_page_cache-help">Dynamic Page Cache module</a>.', [':dynamic_page_cache-help' => (\Drupal::moduleHandler()->moduleExists('dynamic_page_cache')) ? Url::fromRoute('help.page', ['name' => 'dynamic_page_cache'])->toString() : '#']) . '</p>';
      $output .= '<dt>' . t('Configuring the internal page cache') . '</dt>';
      $output .= '<dd>' . t('On the <a href=":cache-settings">Performance page</a>, you can configure how long browsers and proxies may cache pages; that setting is also respected by the DNT Aware Page Cache module. There is no other configuration.', [':cache-settings' => \Drupal::url('system.performance_settings')]) . '</dd>';
      $output .= '</dl>';

      return $output;
  }
}

/**
 * Implements hook_page_attachments().
 * add a clue as a meta tag to the html head
 *
 */
function page_cache_dnt_aware_page_attachments(array &$page) {
    $r = \Drupal::service('request_stack')->getCurrentRequest();
    $DNTstatus = [
        '#tag' => 'meta',
        '#attributes' => [
            'name' => 'dnt',
            'content' => ! empty($r->headers->get('dnt'))? $r->headers->get('dnt'): 0 ,
        ],
    ];
    $page['#attached']['html_head'][] = [$DNTstatus, 'dnt'];
}

/*
 * make whole html page vary on DNT header
 * add a class to the html body tag
 */
function page_cache_dnt_aware_preprocess_html(&$variables) {
    $variables['#cache']['contexts'][] = 'headers:dnt';
    $r = \Drupal::service('request_stack')->getCurrentRequest();
    $status = ! empty($r->headers->get('dnt'))? "dnt": "no-dnt";
    $variables['attributes']['class'][] = $status;
}